#include <boost/interprocess/sync/named_mutex.hpp>
#include <boost/interprocess/shared_memory_object.hpp>
#include <iostream>

using namespace boost::interprocess;

int main() {
    // Create or open shared memory object with name "sh_lab_so" with size of an integer
    shared_memory_object shdmem{open_or_create, "sh_mem_lab_so", read_write};
    shdmem.truncate(sizeof(int));

    // Map shared memory to region in current process's address region
    mapped_region region{shdmem, read_write};

    // Reset count to 0
    int *count = static_cast<int*>(region.get_address());
    *count = 0;

    // Open or create mutex
    named_mutex mutex(open_or_create, "lock_lab_so");

    do
    {
        bool my_turn = rand() % 2;

        if (!my_turn) continue;

        std::cout << "My turn to increment the count!\n";

        mutex.lock();

        std::cout << "Current count value is " << *count << '\n';

        if (*count >= 1000) break;

        std::cout << "Incrementing count to " << *count + 1 << '\n';
        *count = *count + 1;

        mutex.unlock();
    } while (*count < 1000);

    // Clean after our butts
    named_mutex::remove("mutex_lab_so");
    shared_memory_object::remove("shmem_lab_so");

    system("pause");
}