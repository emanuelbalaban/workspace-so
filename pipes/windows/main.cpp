#include <iostream>
#include <vector>
#include <stdlib.h>
#include <sys/types.h>
#include <boost/process/child.hpp>
#include <boost/process/pipe.hpp>
#include <boost/process/io.hpp>

#define N 10
#define LIMIT 10000

namespace bp = boost::process;

bool is_prime(int number)
{
    if (number < 2)
        return false;

    for (int i = 2; i < number / 2; i++)
        if (number % i == 0)
            return false;
    return true;
}

std::vector<int> find_prime_numbers(int interval, int interval_length)
{
    std::vector<int> numbers;

    for (int i = interval; i < interval + interval_length; i++)
        if (is_prime(i))
            numbers.push_back(i);

    return numbers;
}

int main(int argc, char **argv)
{
    int i, interval, count;
    bp::pstream pipes[N][2]; // N pipes
    bp::child processes[N];  // N children

    // Calculate length of interval
    const int interval_length = LIMIT / N;

    // Child branch
    if (argv[1])
    {
        // Read interval
        std::cin >> interval;

        // Find prime numbers
        std::vector<int> prime_numbers = find_prime_numbers(interval, interval_length);

        // Write number of prime numbers found
        int size = prime_numbers.size();
        std::cout << size;

        // Write prime numbers to pipe
        for (int i = 0; i < size; i++)
            std::cout << prime_numbers[i];

        exit(0);
    }

    // Create pipes and children
    for (i = 0; i < N; i++)
    {
        processes[i] = bp::child(argv[0], bp::std_out > pipes[i][0], bp::std_in < pipes[i][1], true);

        // Write interval for each child
        interval = 1 + interval_length * i;
        pipes[i][1] << interval;
        pipes[i][1].close();
    }

    // Parent
    std::vector<int> prime_numbers;
    int status, number;

    // Collect prime numbers
    for (i = 0; i < N; i++)
    {
        // Read number of primes found from pipe
        pipes[i][0] >> count;

        // Read all numbers found by child i
        while (count > 0)
        {
            count--;

            pipes[i][0] >> number;
            prime_numbers.push_back(number);

            std::cout << number << ' ';
        }

        pipes[i][0].close();
        processes[i].terminate();
    }

    std::cout << '\n';
    return 0;
}