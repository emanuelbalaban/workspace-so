#include <iostream>
#include <vector>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

#define N 10
#define LIMIT 10000

using namespace std;

bool is_prime(int number)
{
    if (number < 2)
        return false;

    for (int i = 2; i < number / 2; i++)
        if (number % i == 0)
            return false;
    return true;
}

vector<int> find_prime_numbers(int interval, int interval_length)
{
    vector<int> numbers;

    for (int i = interval; i < interval + interval_length; i++)
        if (is_prime(i))
            numbers.push_back(i);

    return numbers;
}

int main(int argc, char **argv)
{
    int i, interval, count;
    int pfd[N][2]; // N pipes
    int pid[N];    // N children fork ids

    // Calculate length of interval
    const int interval_length = LIMIT / N;

    // Create pipes
    for (i = 0; i < N; i++)
        if (pipe(pfd[i]) < 0)
        {
            cout << "Error creating pipes.\n";
            exit(0);
        }
        else
        {
            // Write interval for each child
            interval = 1 + interval_length * i;
            write(pfd[i][1], &interval, sizeof(interval));
        }

    // Create children and process prime numbers
    for (i = 0; i < N; i++)
        if ((pid[i] = fork()) == 0)
        {
            int *pipe = pfd[i];

            // Read interval
            read(pipe[0], &interval, sizeof(interval));
            cout << "#" << i << " - interval = [" << interval << ", " << interval + interval_length - 1 << "]\n";

            // Find prime numbers
            vector<int> prime_numbers = find_prime_numbers(interval, interval_length);

            // Write number of prime numbers found
            int size = prime_numbers.size();
            write(pipe[1], &size, sizeof(size));

            // Write prime numbers to pipe
            for (int i = 0; i < size; i++)
                write(pipe[1], &prime_numbers[i], sizeof(int));

            exit(0);
        }

    // Parent
    vector<int> prime_numbers;
    int status, number;

    // Wait processes to finish
    for (i = 0; i < N; i++)
        waitpid(pid[i], &status, 0);

    // Collect prime numbers
    for (i = 0; i < N; i++)
    {
        int *pipe = pfd[i];

        // Read number of primes found from pipe
        read(pipe[0], &count, sizeof(int));

        // Read all numbers found by child i
        while (count > 0)
        {
            count--;

            read(pipe[0], &number, sizeof(int));
            prime_numbers.push_back(number);

            cout << number << ' ';
        }
    }

    cout << '\n';
    return 0;
}